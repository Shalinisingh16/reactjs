
import React from 'react';
import Card from './Card';
import "./index.css";
function App()
{
  return(
  <>
  <h1 className="heading">Diseases</h1>
  <Card  imgsrc="https://www.cdc.gov/polio/us/images/phil-22910.jpg"
  title="POLIO" 
  year="A virus that may cause paralysis and is easily preventable by the polio vaccine."
  link="https://en.wikipedia.org/wiki/Polio" />
  <Card imgsrc="https://image.shutterstock.com/image-photo/fever-closeup-medical-thermometer-parent-260nw-1507242380.jpg"
  title="Fever"
  year="A fever is when a person's body temperature is hotter than 37.5 degrees Celsius (99.5 Fahrenheit)."
  link="https://en.wikipedia.org/wiki/Fever"/>
  <Card imgsrc="https://image.shutterstock.com/image-photo/health-care-concept-bellyache-indigestion-260nw-794065012.jpg"
  title="Stomach-ache"
  year="Pain from inside the abdomen or the outer muscle wall, ranging from mild and temporary to severe and requiring emergency care."
  link=" https://en.wikipedia.org/?title=Stomachache&redirect=no"/>
  <Card imgsrc="https://image.shutterstock.com/image-illustration/vibrio-cholerae-bacteria-3d-illustration-260nw-1181713012.jpg"
  title="Cholera"
  year="Cholera is an infection of the small intestine by some strains of the bacterium Vibrio cholerae"
  link="https://en.wikipedia.org/wiki/Cholera" />
  <Card imgsrc="https://image.shutterstock.com/image-vector/illustration-typhoid-fever-infographics-pathogen-260nw-423126832.jpg"
  title="Typhoid"
  year="Typhoid fever is an infection that spreads through contaminated food and water"
  link="https://en.wikipedia.org/wiki/Typhoid_fever" /> 
  <Card imgsrc="https://www.cdc.gov/chickenpox/images/people_varicella2.jpg"
  title="Chickenpox"
  year="Chickenpox, also known as varicella, is a highly contagious disease caused by the initial infection with varicella zoster virus (VZV)."
  link="https://en.wikipedia.org/wiki/Chickenpox" />
  <Card imgsrc="https://images.indianexpress.com/2018/06/suicides-759.jpg"
  title="Depression"
    year="Depression is a state of low mood and aversion to activity. It can affect a person's thoughts, behavior, motivation, feelings, and sense of well-being. "
  link="https://en.wikipedia.org/wiki/Depression_(mood)" />
  <Card imgsrc="https://static.toiimg.com/thumb/msid-70977815,imgsize-775973,width-800,height-600,resizemode-75/70977815.jpg"
  title="Migrane"
  year="Migraine is a primary headache disorder characterized by recurrent headaches that are moderate to severe"
  link="https://en.wikipedia.org/wiki/Migraine" />
  <Card imgsrc="https://cdn-w.medlife.com/2018/06/all-about-diabetes-types-causes-symptoms-treatment.jpg"
  title="Diabetes"
  year="Diabetes mellitus (DM), commonly known as diabetes, is a group of metabolic disorders characterized by a high blood sugar level over a prolonged period of time"
  link="https://en.wikipedia.org/wiki/Diabetes" />
  <Card imgsrc="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Thyroid_system.svg/800px-Thyroid_system.svg.png"
  title="Thyroid"
  year="The thyroid, or thyroid gland, is an endocrine gland in the neck consisting of two connected lobes."
  link="https://en.wikipedia.org/wiki/Thyroid" />
 <Card imgsrc="https://www.news-medical.net/image.axd?picture=2020%2F3%2F%40shutterstock_740932939.jpg" 
  title="Influenza"
  year="A virus causes influenza in birds and some mammals, and is the only species of the genus Alphainfluenzavirus of the virus family Orthomyxoviridae"
  link="https://en.wikipedia.org/wiki/Influenza" />
 <Card imgsrc="https://images.medicinenet.com/images/slideshow/eye_diseases_and_cond_s4_cataracts_2.jpg" 
  title="Cataract"
  year="A cataract is an opacification of the lens of the eye which leads to a decrease in vision. Cataracts often develop slowly and can affect one or both eyes"
  link="https://en.wikipedia.org/wiki/Cataract" />
  <Card imgsrc="https://img.emedihealth.com/wp-content/uploads/2020/03/asthma-treatment-feat.jpg"
  title="Asthma"
  year="Asthma is a long-term inflammatory disease of the airways of the lungs. It is characterized by variable and recurring symptoms"
  link="https://en.wikipedia.org/wiki/Asthma" />
<Card imgsrc="https://images.medicinenet.com/images/image_collection/anatomy/kidney-stone.jpg"
 title="Kidney stone"
  year="Kidney stone disease, also known as nephrolithiasis or urolithiasis, is when a solid piece of material (kidney stone) develops in the urinary tract."
  link="https://en.wikipedia.org/wiki/Kidney_stone_disease"/>
  <Card imgsrc="https://scx2.b-cdn.net/gfx/news/2018/alzheimersvs.jpg"
  title="Alzheimer"
  year="Alzheimer's disease (AD), also referred to simply as Alzheimer's, is a chronic neurodegenerative disease that usually starts slowly and gradually worsens over time"
  link="https://en.wikipedia.org/wiki/Alzheimer%27s_disease" />
  <Card imgsrc="https://www.heart.org/-/media/images/news/2019/january-2019/0131statupdate_sc.jpg" 
  title="Cardiovascular"
  year="Cardiovascular disease (CVD) is a class of diseases that involve the heart or blood vessels.CVD includes coronary artery diseases (CAD) such as angina and myocardial infarction commonly known as a heart attack"
  link="https://en.wikipedia.org/wiki/Cardiovascular_disease" />
</>
);}
export default App; 
